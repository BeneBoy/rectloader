#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include<vector>
#include<sstream>

#include "RectangleList.h"

class FileParser
{
private:

public:
	FileParser();
	static RectangleList ReadFile(string filepath);
	static void WriteFile(string filepath, RectangleList rectlist);
};

