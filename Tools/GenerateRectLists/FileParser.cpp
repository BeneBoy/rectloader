#include "FileParser.h"

FileParser::FileParser()
{
}

RectangleList FileParser::ReadFile(string filepath)
{
	ifstream file(filepath);
	string str;
	Rect tmp;
	RectangleList tmpList;
	
	while (getline(file, str)) 
	{
		vector<string> result;
		stringstream s_stream(str); //create string stream from the string
		while (s_stream.good()) {
			string substr;
			getline(s_stream, substr, ';'); //get first string delimited by comma
			result.push_back(substr);
		}
		tmpList.AddItem(Rect(stoi(result.at(0)), stoi(result.at(1)), stoi(result.at(2))));
	}
	return tmpList;
}

void FileParser::WriteFile(string filepath, RectangleList rectlist)
{
	ofstream file;
	file.open(filepath);
	int count = 0;
	Rect tmp;

	while (count < rectlist.GetSize())
	{
		tmp = rectlist.GetItem(count);
		file << tmp.GetWidth() << ";" << tmp.GetHeigth() << ";" << tmp.GetID() << "\n";
		count++;
	}

	file.close();

	return;
}
