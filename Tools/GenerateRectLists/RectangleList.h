#pragma once

#include <iostream> 
#include <vector>
#include <algorithm>    
#include "Rectangle.h"

using namespace std;

class RectangleList 
{
private:
	vector<Rect> _rectangleList;
	int _side;

public:
	RectangleList();
	RectangleList(vector<Rect> list);
	vector<Rect> GetRectangeList();
	void ClearList();
	void AddItem(Rect);
	size_t GetSize();
	Rect GetItem(int number);
	Rect GetItemByID(int id);

	void SetItemTop(int number, int value);
	int GetItemTop(int number);
	void SetItemLeft(int number, int value);
	int GetItemLeft(int number);

	int GetItemHeight(int number);
	int GetItemWidht(int number);

	int GetSide();
	void SetSide(int value);

	void SortHeightAndWidth();
};

