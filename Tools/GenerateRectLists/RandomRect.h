#pragma once
#include "Rectangle.h"
#include <random>

class RandomRect
{
private:
	static int GenerateNumber(int min, int max);
public:
	static Rect GetRandomRectangle(int min, int max, int id);
};

