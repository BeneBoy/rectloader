#include "Rectangle.h"

Rect::Rect()
{
	_width = 0;
	_height = 0;
	_ID = 0;
	_top = 0;
	_left = 0;
}

Rect::Rect(int width, int height, int ID)
{
	_width = width;
	_height = height;
	_ID = ID;
	_top = 0;
	_left = 0;
}

int Rect::GetWidth()
{
	return _width;
}

void Rect::SetWidth(int value)
{
	_width = value;
}

int Rect::GetHeigth()
{
	return _height;
}

void Rect::SetHeigth(int value)
{
	_height = value;
}

int Rect::GetID()
{
	return _ID;
}

void Rect::SetID(int value)
{
	_ID = value;
}

int Rect::GetTop()
{
	return _top;
}

void Rect::SetTop(int value)
{
	_top = value;
}

int Rect::GetLeft()
{
	return _left;
}

void Rect::SetLeft(int value)
{
	_left = value;
}
