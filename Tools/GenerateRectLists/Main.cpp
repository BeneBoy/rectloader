// GenerateRectLists.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <filesystem>
#include <ctime>

#include "RectangleList.h"
#include "RandomRect.h"
#include "FileParser.h"

using namespace std;

#define NUMBERS_OF_RECTANGLE 999
#define MIN_LENGHT 1
#define MAX_LENGHT 100
#define FILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE.txt"

int main()
{
    RectangleList newList;

    // get new random numbers
    srand((int)time(NULL));

    for (uint64_t i = 0; i < NUMBERS_OF_RECTANGLE; i++)
    {
        newList.AddItem(RandomRect::GetRandomRectangle(MIN_LENGHT, MAX_LENGHT, i));
    }

    FileParser::WriteFile(FILENAME, newList);
    //RectangleList newList2  = FileParser::ReadFile("C:/Users/Bene's Laptop/Desktop/rectloader/BIGFILE.txt");

    cout << "Pause" << endl;
}
