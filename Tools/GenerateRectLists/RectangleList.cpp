#include "RectangleList.h"

RectangleList::RectangleList()
{
	_side = 0;
}

RectangleList::RectangleList(vector<Rect> list)
{
	_side = 0;
	_rectangleList = list;
}

vector<Rect> RectangleList::GetRectangeList()
{
	return _rectangleList;
}

void RectangleList::ClearList()
{
	_rectangleList.clear();
}

void RectangleList::AddItem(Rect rect)
{
	_rectangleList.push_back(rect);
}

size_t RectangleList::GetSize()
{
	return _rectangleList.size();
}

Rect RectangleList::GetItem(int number)
{
	return _rectangleList[number];
}

Rect RectangleList::GetItemByID(int id)
{
	int count = 0;
	while (count < _rectangleList.size())
	{
		if (id == _rectangleList[count].GetID())
		{
			return _rectangleList[count];
		}
		count++;
	}
	return Rect();
}

void RectangleList::SetItemTop(int number, int value)
{
	_rectangleList[number].SetTop(value);

}

int RectangleList::GetItemTop(int number)
{
	return _rectangleList[number].GetTop();
}

void RectangleList::SetItemLeft(int number, int value)
{
	_rectangleList[number].SetLeft(value);
}

int RectangleList::GetItemLeft(int number)
{
	return _rectangleList[number].GetLeft();
}

int RectangleList::GetItemHeight(int number)
{
	return _rectangleList[number].GetHeigth();
}

int RectangleList::GetItemWidht(int number)
{
	return _rectangleList[number].GetWidth();
}

int RectangleList::GetSide()
{
	return _side;
}

void RectangleList::SetSide(int value)
{
	_side = value;
}

void RectangleList::SortHeightAndWidth()
{
	//sort by height and if the height is the same sort by widht
	auto sortRuleLambda = [](Rect& s1, Rect& s2) -> bool
	{
		if (s1.GetHeigth() == s2.GetHeigth())
		{
			if (s1.GetWidth() > s2.GetWidth())
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		return s1.GetHeigth() > s2.GetHeigth();
	};

	sort(_rectangleList.begin(), _rectangleList.end(), sortRuleLambda);
}
