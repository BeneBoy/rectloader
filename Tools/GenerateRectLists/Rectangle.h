#pragma once
class Rect
{
private:
	// a rectangle needs a widht and height 
	// and every rectangle as a ID or name

	int _width;
	int _height;
	int _ID;

	int _top;
	int _left;

public:
	Rect();
	Rect(int width, int height, int ID);

	int GetWidth();
	void SetWidth(int value);

	int GetHeigth();
	void SetHeigth(int value);

	int GetID();
	void SetID(int value);

	int GetTop();
	void SetTop(int value);

	int GetLeft();
	void SetLeft(int value);
};

