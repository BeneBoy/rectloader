# RectLoader

Interview task

## Task

Sort rectangles of different sizes into one square. 


## Generate Rect Lists 
This tool can be used to generate lists with differnt sizes of elements and different count of rectangles.

Just change this defines in the Main.cpp of GeneratreRectLists: 
````
#define NUMBERS_OF_RECTANGLE 999
#define MIN_LENGHT 1
#define MAX_LENGHT 100
#define FILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE.txt"
````

Example File:
The struct is: 
Width ; Heigth ; ID
```
100;82;0
45;5;1
79;54;2
48;81;3
27;93;4
74;16;5
64;93;6
83;28;7
47;16;8
20;32;9
29;64;10
91;72;11
61;48;12
19;71;13
62;14;14
```

## RectLoad
To use this to read a rectangle contained file and pack all the rectangles into one big sqaure. The square should as smalle as possible. 
The output is an html file.

Example:
![FILE html Example](ReadMe/FILEHTML.png)


Define here the in and output files.

```
#define INPUTFILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE.txt"
#define OUTPUTFILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE.HTML"
```


### Big File

This a screenshot from a file with 99999 entrys. 

![FILE html Example](ReadMe/BIGFILEHTML.png)

