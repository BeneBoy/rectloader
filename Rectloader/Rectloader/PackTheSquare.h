#pragma once
#include <vector>

#include "PackLine.h"
#include"../../Tools/GenerateRectLists/RectangleList.h"

class PackTheSquare
{
private: 
	int _sideMax;
	vector<PackLine> _lines;
	void CreateNewLine(int xStart, int y);
	void SetLineY(int line, int y);
	int GetLineY(int line);
	void SetLineX(int line, int x);
	int GetLineX(int line);
	void SetLineXMax(int line, int x);
	int GetLineXMax(int line);

	int GetLineSize();

	bool FitInLine(int line, int widht, int side);

	void AddItemInLine(RectangleList& rectList, int itemNumber, int line);
	void AddItemNextLine(RectangleList& rectList, int itemNumber);

public:
	PackTheSquare();
	void Start(RectangleList& rectList);

};

