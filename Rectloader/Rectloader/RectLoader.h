#pragma once
#include <iostream> 
#include <vector>
#include <string>

#include "../../Tools/GenerateRectLists/RectangleList.h"
#include "../../Tools/GenerateRectLists/FileParser.h"
#include "CreateHml.h"

class RectLoader
{
private:
	string _filePath = "";

public:
	RectLoader(const std::string& filePath);
	std::vector<Rect> load();
	void writeFile(string& filePath, RectangleList& rectList);

};

