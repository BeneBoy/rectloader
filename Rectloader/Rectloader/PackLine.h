#pragma once

class PackLine
{
public:
	PackLine(int x, int y) { xStart = x; yCurrent = y; }
	int xStart;
	int xMax;
	int yCurrent;
};