#include "RectLoader.h"

RectLoader::RectLoader(const std::string& filePath)
{
	_filePath = filePath;
}


std::vector<Rect> RectLoader::load()
{
	RectangleList newList = FileParser::ReadFile(_filePath);

	return newList.GetRectangeList();
}

void RectLoader::writeFile(string& filePath, RectangleList& rectList)
{
	CreateHml::WriteHtmlFile(filePath, rectList);
}

