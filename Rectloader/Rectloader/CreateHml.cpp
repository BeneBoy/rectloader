#include "CreateHml.h"

CreateHml::CreateHml()
{
}

bool CreateHml::GetHtmlHead(vector<string>& fileStrings,int side)
{
	fileStrings.push_back("<!DOCTYPE html>");
	fileStrings.push_back("<html> ");
	fileStrings.push_back("<head>");
	fileStrings.push_back("<title>RectLoader</title>");
	fileStrings.push_back("<style>");
	fileStrings.push_back("div.relative {");
	fileStrings.push_back("  position: relative;");
	fileStrings.push_back("  width: " + to_string(side) + "px;");
	fileStrings.push_back("  height: " + to_string(side) + "px;");
	fileStrings.push_back("  border: 3px solid #000000;");
	fileStrings.push_back("  background-color: floralwhite;");
	fileStrings.push_back("} ");
	fileStrings.push_back("div.absolute_green {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid #73AD21;");
	fileStrings.push_back("  background-color: #73AD21;");
	fileStrings.push_back("  ");
	fileStrings.push_back("}");
	fileStrings.push_back("div.absolute_yellow {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid #dfd330;");
	fileStrings.push_back("  background-color: #dfd330;");
	fileStrings.push_back("}");
	fileStrings.push_back("div.absolute_blue {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid #08b7ec;");
	fileStrings.push_back("  background-color: #08b7ec;");
	fileStrings.push_back("}");
	fileStrings.push_back("div.absolute_red {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid #ec0808;");
	fileStrings.push_back("  background-color: #ec0808;");
	fileStrings.push_back("}");
	fileStrings.push_back("div.absolute_purple {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid purple;");
	fileStrings.push_back("  background-color: purple;");
	fileStrings.push_back("}");
	fileStrings.push_back("div.absolute_pink {");
	fileStrings.push_back("  position: absolute;");
	fileStrings.push_back("  border: 0px solid pink;");
	fileStrings.push_back("  background-color: pink;");
	fileStrings.push_back("}");
	fileStrings.push_back("</style>");
	fileStrings.push_back("</head>");
	fileStrings.push_back("<body>");
	fileStrings.push_back("");
	fileStrings.push_back("<h1>Rect Loader</h1>");
	fileStrings.push_back("");
	fileStrings.push_back("<div class=\"relative\">");

	return true;
}

string CreateHml::CreateHtmlRect(Rect object)
{
	int randomValue = 0;
	string color = "";

	randomValue = rand() % 6 + 1;

	switch (randomValue)
	{
	case 1:
		color = "absolute_green";
		break;
	case 2:
		color = "absolute_yellow";
		break;
	case 3:
		color = "absolute_blue";
		break;
	case 4:
		color = "absolute_red";
		break;
	case 5:
		color = "absolute_purple";
		break;
	case 6:
		color = "absolute_pink";
		break;
	default:

		color = "absolute_green";
		break;
	}


	return "<div class = \""+ color +"\" style=\"top:" + to_string(object.GetTop()) + "px;left:" + to_string(object.GetLeft()) + "px;width:" + to_string(object.GetWidth()) + "px;height:" + to_string(object.GetHeigth()) + "px;\">" + to_string(object.GetID()) + "</div>";
}

bool CreateHml::GetHtmlEnd(vector<string>& fileStrings)
{
	fileStrings.push_back("</div>");
	fileStrings.push_back("</body>");
	fileStrings.push_back("</html>");

	return true;
}

void CreateHml::WriteHtmlFile(string& filepath, RectangleList rectList)
{
	ofstream file;
	file.open(filepath);
	int count = 0;
	vector<string> htmlFileStringList;
	
	GetHtmlHead(htmlFileStringList,rectList.GetSide());

	while (count < rectList.GetSize())
	{
		htmlFileStringList.push_back(CreateHtmlRect(rectList.GetItem(count)));
		count++;
	}

	GetHtmlEnd(htmlFileStringList);

	count = 0;
	while (count < htmlFileStringList.size())
	{
		file << htmlFileStringList[count] + "\n";
		count++;
	}

	file.close();


}