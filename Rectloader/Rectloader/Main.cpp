// Rectloader.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "RectLoader.h"
#include "PackTheSquare.h"


#define INPUTFILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE3.txt"
#define OUTPUTFILENAME "C:/Users/Bene's Laptop/Desktop/rectloader/FILE3.HTML"

int main()
{
    // Read File
    string filepathInput = INPUTFILENAME;
    string filepathOutput = OUTPUTFILENAME;
    RectLoader rectLoader(INPUTFILENAME);
    RectangleList rectList(rectLoader.load());

    PackTheSquare packTheSquare;

    // Sort rect from biggest height to smallest heigth
    rectList.SortHeightAndWidth();

    // give each element a place in the square
    packTheSquare.Start(rectList);

    // Write file
    rectLoader.writeFile(filepathOutput, rectList);
}
