#include "PackTheSquare.h"

PackTheSquare::PackTheSquare()
{
}

void PackTheSquare::CreateNewLine(int xStart, int y)
{
    _lines.push_back(PackLine(xStart, y));
}

void PackTheSquare::SetLineY(int line, int y)
{
    _lines[line].yCurrent = y;
}

int PackTheSquare::GetLineY(int line)
{
    return _lines[line].yCurrent;
}

void PackTheSquare::SetLineX(int line, int x)
{
    _lines[line].xStart = x;
}

int PackTheSquare::GetLineX(int line)
{
    return _lines[line].xStart;
}

void PackTheSquare::SetLineXMax(int line, int x)
{
    _lines[line].xMax= x;
}

int PackTheSquare::GetLineXMax(int line)
{
    return _lines[line].xMax;
}

int PackTheSquare::GetLineSize()
{
    return _lines.size();
}

bool PackTheSquare::FitInLine(int line, int widht, int side)
{
    bool result = false;


    int newYCurrent = GetLineY(line) + widht;
    if (newYCurrent < side)
    {
        result = true;
    }

    return result;
}

void PackTheSquare::AddItemInLine(RectangleList& rectList, int itemNumber, int line)
{
    rectList.SetItemTop(itemNumber, GetLineX(line));
    rectList.SetItemLeft(itemNumber, GetLineY(line));

    SetLineY(line, GetLineY(line) + rectList.GetItemWidht(itemNumber));
}

void PackTheSquare::AddItemNextLine(RectangleList& rectList, int itemNumber)
{
    // we ne a nextline
    // letzte line max!
    int newLine = GetLineSize();
    CreateNewLine(GetLineXMax(GetLineSize() - 1), 0);
    
    rectList.SetItemTop(itemNumber, GetLineX(newLine));
    rectList.SetItemLeft(itemNumber, 0);

    SetLineY(newLine, GetLineY(newLine) + rectList.GetItemWidht(itemNumber));
    SetLineXMax(newLine, (GetLineX(newLine) + rectList.GetItemHeight(itemNumber)));

}

void PackTheSquare::Start(RectangleList& rectList)
{
    Rect tmp;
    int count = 0;
    int currentSide = 0;
    int lineCounter = 1;
    
    
    // define the first and second item
    rectList.SetItemTop(0, 0);
    rectList.SetItemLeft(0, 0);
    currentSide = rectList.GetItemWidht(0);
    CreateNewLine(0, rectList.GetItemWidht(0));
    SetLineXMax(0, rectList.GetItemHeight(0));
    
    AddItemNextLine(rectList, 1);

    if ((GetLineX(1) + rectList.GetItemHeight(1)) > currentSide)
    {
        currentSide = GetLineX(1) + rectList.GetItemHeight(1);
    }
    lineCounter++;
    
    count = 2;

    while (count < rectList.GetSize())
    {
        int getItemWidht = rectList.GetItemWidht(count);
        int currentLine = 0;
        int bestLine = 0; 
        int bestLineIncrease = getItemWidht;
        bool doesItFit = false;
        lineCounter = 0;

        bool placeFound = true;
        // go through all lines ! if this fit into the line without increase side do it
        while (placeFound)
        {
            if (FitInLine(lineCounter,getItemWidht, currentSide))
            {
                // add to line

                AddItemInLine(rectList, count, lineCounter);
                //rectList.SetItemTop(count, GetLineX(lineCounter));
                //rectList.SetItemLeft(count, GetLineY(lineCounter));
                //SetLineY(lineCounter, GetLineY(lineCounter) + rectList.GetItemWidht(count));
                //_lines[lineCounter].yCurrent += rectList.GetItemWidht(count);
                doesItFit = true;
            }
            else
            {
                // did not fit
                // but increase site maybe the smallest?
                if (lineCounter > 0)
                {
                    int test = GetLineY(lineCounter) - currentSide;
                    int test2 = GetLineY(bestLine) - currentSide;
                    if (test2 > test)
                    {
                        bestLine = lineCounter;
                    }
                }
               
            }
            lineCounter++;

            if (lineCounter >= GetLineSize() || doesItFit)
            {
                placeFound = false;
            }
        }

        // if no check what cost more! increase height or widht! to the better thing!

        if(!doesItFit)
        {
            int bestWidhtIncrease = getItemWidht + GetLineY(bestLine);
            int bestHeightIncrease = rectList.GetItemHeight(count) + GetLineXMax(GetLineSize() - 1) ;
            if (bestWidhtIncrease < bestHeightIncrease)
            {
                // attach in bestline
                AddItemInLine(rectList, count, bestLine);

              /*  rectList.SetItemTop(count, GetLineX(bestLine));
                rectList.SetItemLeft(count, GetLineY(bestLine));
                SetLineY(bestLine, GetLineY(bestLine) + rectList.GetItemWidht(count));*/
                currentSide = GetLineY(bestLine);
                printf("current Side %d", currentSide);
            }
            else
            {
                // attach nextline
                AddItemNextLine(rectList, count);


               /* CreateNewLine(GetLineX(GetLineSize() - 1) + rectList.GetItemHeight(count), 0);
                rectList.SetItemTop(count, GetLineX(GetLineSize() - 1));
                rectList.SetItemLeft(count, 0);
                SetLineY(rectList.GetItemLeft(count), rectList.GetItemLeft(count));*/

                //currentSide = GetLineXMax(GetLineSize() - 1) + rectList.GetItemHeight(count);
                currentSide = GetLineXMax(GetLineSize() - 1);
            }

           
        }
        count++;
    }

    rectList.SetSide(currentSide);
}
