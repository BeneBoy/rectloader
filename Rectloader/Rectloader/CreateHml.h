#pragma once
#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>

#include"../../Tools/GenerateRectLists/RectangleList.h"

class CreateHml
{
private:
	static bool GetHtmlHead(vector<string>& fileStrings,int side);
	static string CreateHtmlRect(Rect object);
	static bool GetHtmlEnd(vector<string>& fileStrings);
public:
	CreateHml();
	static void WriteHtmlFile(string& filepath, RectangleList rectList);
};

